# Pizza plaza project

## Indications

- In order to run this project I have been using this docker image: https://github.com/wodby/docker4php
    - PHP_TAG=8.1-dev
    - NGINX_TAG=1.23-5.30.2
    - MARIADB_TAG=10.9-3.24.2
    
- I have relied on Symfony 5.4 and latests versions of bootstrap and jquery

- I chose to develop pizza-plaza project as a separated folder inside docker4php folder, that is to say: docker4php folder contains pizza-plaza project

- Inside piza-plaza project there must be a .env file. In order to work, It must contain these variables: 

    - APP_ENV=dev
    - APP_SECRET=2ca64f8d83b9e89f5f19d672841d6bb8 (it may vary)
    - DATABASE_URL="mysql://php:php@mariadb/php?serverVersion=mariadb-10.5.8" (the options you have decided regarding docker configuration)
    - LEGACY_INDEX_FOLDER="../pages/index.php"
    - MESSENGER_TRANSPORT_DSN=doctrine://default
    - BASE_URL="http://dev.docker.local/" (for example)
    
- After building the project (docker-compose up -d), composer install must be launched

- Migrations in folder pizza-plaza/migrations must be launched (php bin/console doctrine:migrations:migrate)

- data_to_import.sql must be launched 

- To access online ordering, the path is /online_ordering

- To access admin panel, the access is /admin

- Before you can enter admin, there will be a login. Example data of user are: 
    - email: php@gmail.com
    - password: php
    
- At organising controllers, entities, services, etc I tried to follow a DDD patron


