<?php require_once('component.php'); ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pizza Plaza</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="../public/assets/css/main.min.css') }}" rel="stylesheet" />
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="?site=main">Pizza Plaza</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item <?php if($currentSite === 'main') echo 'active'; ?>">
                <a class="nav-link" href="?site=main">Startseite</a>
            </li>
            <li class="nav-item <?php if($currentSite === 'about') echo 'active'; ?>">
                <a class="nav-link" href="?site=about">&Uuml;ber uns</a>
            </li>
            <li class="nav-item <?php if($currentSite === 'contact') echo 'active'; ?>">
                <a class="nav-link" href="?site=contact">Kontakt</a>
            </li>
            <li class="nav-item <?php if($currentSite === 'imprint') echo 'active'; ?>">
                <a class="nav-link" href="?site=imprint">Impressum</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/online_ordering">Online Ordering</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin">Go to Administration</a>
            </li>
        </ul>
    </div>
</nav>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">
                            <?php

                            $numItems = count($generatedBreadcrumb);
                            $i = 0;

                            foreach ($generatedBreadcrumb as $breadcrumb)
                            {
                                echo '<li class="breadcrumb-item" aria-current="page">';

                                if(++$i !== $numItems && $breadcrumb->hasUrl()) {
                                    echo '<a href="' . $breadcrumb->getUrl() . '">' . $breadcrumb->getTitle() . '</a>';
                                } else {
                                    echo $breadcrumb->getTitle();
                                }

                                echo '</li>';
                            }
                            ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </ol>
</nav>


<div class="container">
    <?php include $currentSite . '.php'; ?>
</div>

<script src="https://code.jquery.com/jquery-3.6.4.min.js" integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="../public/assets/js/main.js"></script>
</body>
</html>
