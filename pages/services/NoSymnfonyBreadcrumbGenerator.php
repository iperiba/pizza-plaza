<?php

namespace InheritedSite\services;

use Naucon\Breadcrumbs\Breadcrumbs;

class NoSymnfonyBreadcrumbGenerator
{
    const HOME_EXTERNAL_NAME = "Home";

    private $breadcrumb;
    private $baseUrl;

    function __construct(string $baseUrl)
    {
        $this->breadcrumb = new Breadcrumbs();
        $this->baseUrl = $baseUrl;
    }

    function generateBreadcrumbPrependHome(array $breadcrumbArray)
    {
        $this->breadcrumb->add(self::HOME_EXTERNAL_NAME, $this->baseUrl);

        if (empty($breadcrumbArray)) {
            return $this->breadcrumb;
        }

        foreach ($breadcrumbArray as $index => $value) {
            if ($index !== self::HOME_EXTERNAL_NAME) {
                $this->breadcrumb->add($index, $value);
            }
        }

        return $this->breadcrumb;
    }
}