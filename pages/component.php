<?php

require_once('../../vendor/autoload.php');

use InheritedSite\services\NoSymnfonyBreadcrumbGenerator;

const SITE_VARIABLE_NAME = "site";

$host = (empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]";
$completeUrl = $host.$_SERVER['REQUEST_URI'];

$breadcrumbGenerator = new NoSymnfonyBreadcrumbGenerator($host);
$arrayParameters = (isset($_GET[SITE_VARIABLE_NAME])) ? array(ucfirst($_GET[SITE_VARIABLE_NAME]) => $completeUrl) : array();

$generatedBreadcrumb = $breadcrumbGenerator->generateBreadcrumbPrependHome($arrayParameters);












