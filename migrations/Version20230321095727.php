<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230321095727 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE customers (id INT AUTO_INCREMENT NOT NULL, firstName VARCHAR(255) DEFAULT NULL, lastName VARCHAR(255) DEFAULT NULL, address_street VARCHAR(255) NOT NULL, address_street_number INT NOT NULL, address_zip VARCHAR(255) NOT NULL, address_city VARCHAR(255) NOT NULL, phone_phone_number VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE extras (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, isChoosable TINYINT(1) DEFAULT NULL, price_amount DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orderedPizzas (id INT AUTO_INCREMENT NOT NULL, order_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', pizza_id INT DEFAULT NULL, INDEX IDX_70409BA38D9F6D38 (order_id), INDEX IDX_70409BA3D41D1D42 (pizza_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orderedPizzas_extras (extra_id INT NOT NULL, orderedPizza_id INT NOT NULL, INDEX IDX_ED8655F46C990D72 (orderedPizza_id), INDEX IDX_ED8655F42B959FC6 (extra_id), PRIMARY KEY(orderedPizza_id, extra_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', customer_id INT DEFAULT NULL, created_at DATETIME NOT NULL, finished_at DATETIME DEFAULT NULL, total_price DOUBLE PRECISION DEFAULT \'0\' NOT NULL, INDEX IDX_E52FFDEE9395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pizzas (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, price_amount DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pizzas_extras (pizza_id INT NOT NULL, extra_id INT NOT NULL, INDEX IDX_D69E2D01D41D1D42 (pizza_id), INDEX IDX_D69E2D012B959FC6 (extra_id), PRIMARY KEY(pizza_id, extra_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE orderedPizzas ADD CONSTRAINT FK_70409BA38D9F6D38 FOREIGN KEY (order_id) REFERENCES orders (id)');
        $this->addSql('ALTER TABLE orderedPizzas ADD CONSTRAINT FK_70409BA3D41D1D42 FOREIGN KEY (pizza_id) REFERENCES pizzas (id)');
        $this->addSql('ALTER TABLE orderedPizzas_extras ADD CONSTRAINT FK_ED8655F46C990D72 FOREIGN KEY (orderedPizza_id) REFERENCES orderedPizzas (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE orderedPizzas_extras ADD CONSTRAINT FK_ED8655F42B959FC6 FOREIGN KEY (extra_id) REFERENCES extras (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEE9395C3F3 FOREIGN KEY (customer_id) REFERENCES customers (id)');
        $this->addSql('ALTER TABLE pizzas_extras ADD CONSTRAINT FK_D69E2D01D41D1D42 FOREIGN KEY (pizza_id) REFERENCES pizzas (id)');
        $this->addSql('ALTER TABLE pizzas_extras ADD CONSTRAINT FK_D69E2D012B959FC6 FOREIGN KEY (extra_id) REFERENCES extras (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE orderedPizzas DROP FOREIGN KEY FK_70409BA38D9F6D38');
        $this->addSql('ALTER TABLE orderedPizzas DROP FOREIGN KEY FK_70409BA3D41D1D42');
        $this->addSql('ALTER TABLE orderedPizzas_extras DROP FOREIGN KEY FK_ED8655F46C990D72');
        $this->addSql('ALTER TABLE orderedPizzas_extras DROP FOREIGN KEY FK_ED8655F42B959FC6');
        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEE9395C3F3');
        $this->addSql('ALTER TABLE pizzas_extras DROP FOREIGN KEY FK_D69E2D01D41D1D42');
        $this->addSql('ALTER TABLE pizzas_extras DROP FOREIGN KEY FK_D69E2D012B959FC6');
        $this->addSql('DROP TABLE customers');
        $this->addSql('DROP TABLE extras');
        $this->addSql('DROP TABLE orderedPizzas');
        $this->addSql('DROP TABLE orderedPizzas_extras');
        $this->addSql('DROP TABLE orders');
        $this->addSql('DROP TABLE pizzas');
        $this->addSql('DROP TABLE pizzas_extras');
    }
}
