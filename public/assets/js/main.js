// TODO: implement some nice JavaScript

$(document).ready(function() {
    $(".extra-collapse").collapse();

    $("form[id^=pizzaForm]").submit(function( event ) {
        ajaxRequest($(this), event, "/add_pizza", "POST", "#extra_selection", "#receipt")
    });

    $("form[id^=extraForm]").submit(function( event ) {
        ajaxRequest($(this), event,"/add_extra", "POST", "#extra_selection", "#receipt", "#extra_selection_form_idOrderedPizza");
    });

    $("form[id^=removeOrderedPizzaForm]").submit(function( event ) {
        ajaxRequest($(this), event,"/remove_ordered_pizza", "POST", "#extra_selection", "#receipt");
    });

    function ajaxRequest(form, event, url, type, extrasFormBlock, receiptBlock, idFormSelector) {
        event.preventDefault();
        let data = {};

        if (url === '/add_extra') {
            let formId = form.attr('id');
            let expression = '#' + formId + ' :input:checked';
            let inputs = $(expression);
            let values = [];

            inputs.each(function( index, element ) {
                values.push(element.value);
            });

            data['pizzaExtrasId'] = values;
            let expression02 = '#' + formId + ' ' + idFormSelector;
            data['idOrderedPizza'] = $(expression02).attr("value");
        } else {
            form.serializeArray().forEach((object)=>{
                data[object.name] = object.value;
            });
        }

        $.ajax({
            url: url,
            data: data,
            type: type,
            success: function(data, textStatus, jqXHR) {
                let jsonData = JSON.parse(data);

                if (!(jsonData instanceof Object)) {
                    return;
                }

                if (!(jsonData.hasOwnProperty('extraFormsHtml'))) {
                    return;
                }

                let selectionExtrasFormBlock = $(extrasFormBlock);
                selectionExtrasFormBlock.empty();
                selectionExtrasFormBlock.append(jsonData.extraFormsHtml);

                if (!(jsonData.hasOwnProperty('receiptHtml'))) {
                    return;
                }

                let selectionReceiptBlock = $(receiptBlock);
                selectionReceiptBlock.empty();
                selectionReceiptBlock.append(jsonData.receiptHtml);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        })
    }
});

