<?php

namespace App\Application\Events;

use App\Application\DTOs\ApplicationDTOInterface;

class OrderConfirmed extends ApplicationEventAbstractClass
{
    public const NAME = 'order.confirmed';

    protected $orderConfirmedDTO;
    private $persistedOrder;
    private $persistedCustomer;

    public function __construct(ApplicationDTOInterface $orderConfirmedDTO)
    {
        $this->orderConfirmedDTO = $orderConfirmedDTO;
    }

    /**
     * @return ApplicationDTOInterface
     */
    public function getOrderConfirmedDTO(): ApplicationDTOInterface
    {
        return $this->orderConfirmedDTO;
    }

    /**
     * @param ApplicationDTOInterface $orderConfirmedDTO
     */
    public function setOrderConfirmedDTO(ApplicationDTOInterface $orderConfirmedDTO): void
    {
        $this->orderConfirmedDTO = $orderConfirmedDTO;
    }

    /**
     * @return mixed
     */
    public function getPersistedOrder()
    {
        return $this->persistedOrder;
    }

    /**
     * @param mixed $persistedOrder
     */
    public function setPersistedOrder($persistedOrder): void
    {
        $this->persistedOrder = $persistedOrder;
    }

    /**
     * @return mixed
     */
    public function getPersistedCustomer()
    {
        return $this->persistedCustomer;
    }

    /**
     * @param mixed $persistedCustomer
     */
    public function setPersistedCustomer($persistedCustomer): void
    {
        $this->persistedCustomer = $persistedCustomer;
    }
}