<?php

namespace App\Application\Events;

class OrderedPizzaRemoved extends ApplicationEventAbstractClass
{
    public const NAME = 'ordered.pizza.removed';

    private $orderedPizzaId;

    public function __construct(string $orderedPizzaId)
    {
        $this->orderedPizzaId = $orderedPizzaId;
    }

    /**
     * @return string
     */
    public function getOrderedPizzaId(): string
    {
        return $this->orderedPizzaId;
    }

    /**
     * @param string $orderedPizzaId
     */
    public function setOrderedPizzaId(string $orderedPizzaId): void
    {
        $this->orderedPizzaId = $orderedPizzaId;
    }
}