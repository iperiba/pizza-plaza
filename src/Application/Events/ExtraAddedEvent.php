<?php

namespace App\Application\Events;

use App\Application\DTOs\ApplicationDTOInterface;

class ExtraAddedEvent extends ApplicationEventAbstractClass
{
    public const NAME = 'extra.added';

    protected $addExtraAjaxDTO;

    public function __construct(ApplicationDTOInterface $addExtraAjaxDTO)
    {
        $this->addExtraAjaxDTO = $addExtraAjaxDTO;
    }

    /**
     * @return ApplicationDTOInterface
     */
    public function getAddExtraAjaxDTO(): ApplicationDTOInterface
    {
        return $this->addExtraAjaxDTO;
    }

    /**
     * @param ApplicationDTOInterface $addExtraAjaxDTO
     */
    public function setAddExtraAjaxDTO(ApplicationDTOInterface $addExtraAjaxDTO): void
    {
        $this->addExtraAjaxDTO = $addExtraAjaxDTO;
    }
}