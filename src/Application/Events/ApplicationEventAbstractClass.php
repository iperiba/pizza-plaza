<?php

namespace App\Application\Events;

use Symfony\Contracts\EventDispatcher\Event;

abstract class ApplicationEventAbstractClass extends Event
{
    private $extraForms;
    private $receipt;

    /**
     * @return mixed
     */
    public function getExtraForms()
    {
        return $this->extraForms;
    }

    /**
     * @param mixed $extraForms
     */
    public function setExtraForms($extraForms): void
    {
        $this->extraForms = $extraForms;
    }

    /**
     * @return mixed
     */
    public function getReceipt()
    {
        return $this->receipt;
    }

    /**
     * @param mixed $receipt
     */
    public function setReceipt($receipt): void
    {
        $this->receipt = $receipt;
    }
}