<?php

namespace App\Application\Events;

use App\Application\DTOs\ApplicationDTOInterface;

class PizzaAddedEvent extends ApplicationEventAbstractClass
{
    public const NAME = 'pizza.added';

    protected $addPizzaAjaxDTO;

    public function __construct(ApplicationDTOInterface $addPizzaAjaxDTO)
    {
        $this->addPizzaAjaxDTO = $addPizzaAjaxDTO;
    }

    /**
     * @return ApplicationDTOInterface
     */
    public function getAddPizzaAjaxDTO(): ApplicationDTOInterface
    {
        return $this->addPizzaAjaxDTO;
    }
}