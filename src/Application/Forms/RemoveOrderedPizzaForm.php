<?php


namespace App\Application\Forms;

use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RemoveOrderedPizzaForm extends AbstractType
{
    const REMOVE_ORDERED_PIZZA_ACTION_URL = 'remove_ordered_pizza';

    private $logger;
    private $router;

    public function __construct(LoggerInterface $logger, UrlGeneratorInterface $router)
    {
        $this->logger = $logger;
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $idOrderedPizza = $options['data']['idOrderedPizza'];

        $builder
            ->add('idOrderedPizza', HiddenType::class, [
                'data' => $idOrderedPizza
            ])
            ->add('pizzaSubmit', SubmitType::class, ['label' => 'Remove pizza'])
            ->setAction($this->router->generate(self::REMOVE_ORDERED_PIZZA_ACTION_URL));
    }
}