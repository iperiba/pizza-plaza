<?php


namespace App\Application\Forms;


use App\Domain\Entities\Extra;
use App\Infrastructure\Services\InfrastructureServiceInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ExtraSelectionForm extends AbstractType
{
    const ADD_EXTRA_ACTION_URL = 'add_extra';

    private $logger;
    private $router;
    private $accentRemover;
    private $idOrderedPizza;

    public function __construct(LoggerInterface $logger, UrlGeneratorInterface $router, InfrastructureServiceInterface $accentRemover)
    {
        $this->logger = $logger;
        $this->router = $router;
        $this->accentRemover = $accentRemover;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $idOrderedPizza = $options['data']['idOrderedPizza'];
        $this->setIdOrderedPizza($idOrderedPizza);
        $pizzaExtras = $options['data']['pizzaExtras'];
        $extrasAlreadySelected = $options['data']['extrasAlreadySelected'];

        $builder
            ->add('idOrderedPizza', HiddenType::class, [
                'data' => $idOrderedPizza
            ])
            ->add('pizzaExtras', ChoiceType::class, [
                'choices' => $pizzaExtras,
                'choice_value' => 'id',
                'choice_label' => function (?Extra $extra) {
                    return $extra ? $extra->getName() . ' - ' . $extra->getPrice()->getAmount() . ' €' : '';
                },
                'choice_attr' => function (?Extra $extra) {
                    return $extra ? ['class' => 'extra_' . strtolower($this->accentRemover->remove_accents($extra->getName()))
                        . '_' . $this->getIdOrderedPizza()] : [];
                },
                'choice_name' => function (?Extra $extra) {
                    return $extra ? 'extra_' . strtolower($this->accentRemover->remove_accents($extra->getName()))
                        . '_' . $this->getIdOrderedPizza() : [];
                },
                'expanded' => true,
                'multiple' => true,
                'data' => $extrasAlreadySelected
            ])
            ->add('pizzaSubmit', SubmitType::class, ['label' => 'Add Extras'])
            ->setAction($this->router->generate(self::ADD_EXTRA_ACTION_URL));
    }

    /**
     * @return mixed
     */
    public function getIdOrderedPizza()
    {
        return $this->idOrderedPizza;
    }

    /**
     * @param mixed $idOrderedPizza
     */
    public function setIdOrderedPizza($idOrderedPizza): void
    {
        $this->idOrderedPizza = $idOrderedPizza;
    }
}