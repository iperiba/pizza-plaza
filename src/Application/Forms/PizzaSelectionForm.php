<?php


namespace App\Application\Forms;


use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PizzaSelectionForm extends AbstractType
{
    const ADD_PIZZA_ACTION_URL = 'add_pizza';

    private $logger;
    private $router;

    public function __construct(LoggerInterface $logger, UrlGeneratorInterface $router)
    {
        $this->logger = $logger;
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $idPizza = $options['data']['idPizza'];
        $numberPizzas = $options['data']['numberPizzas'];
        $idOrder = $options['data']['idOrder'];
        $priceAmount = $options['data']['priceAmount'];

        $builder
            ->add('idPizza', HiddenType::class, [
                'data' => $idPizza
            ])
            ->add('idOrder', HiddenType::class, [
                'data' => $idOrder
            ])
            ->add('priceAmount', HiddenType::class, [
                'data' => $priceAmount
            ])
            ->add('numberPizzas', IntegerType::class, [
                'data' => $numberPizzas,
                'attr' => ['min' => '0']
            ])
            ->add('pizzaSubmit', SubmitType::class, ['label' => 'Add Pizza'])
            ->setAction($this->router->generate(self::ADD_PIZZA_ACTION_URL));
    }
}