<?php


namespace App\Application\Forms;

use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CheckoutCustomerForm extends AbstractType
{
    const CONFIRM_ORDER_ACTION = 'confirm_order';

    private $logger;
    private $router;

    public function __construct(LoggerInterface $logger, UrlGeneratorInterface $router)
    {
        $this->logger = $logger;
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $idOrder = $options['data']['idOrder'];

        $builder
            ->add('idOrder', HiddenType::class, [
                'data' => $idOrder
            ])
            ->add('firstName', TextType::class, [
                'required' => true,
                'trim' => true,
                'label' => 'First name',
            ])
            ->add('lastName', TextType::class, [
                'required' => true,
                'trim' => true,
                'label' => 'Last name',
            ])
            ->add('street', TextType::class, [
                'required' => true,
                'trim' => true,
                'label' => 'Street'
            ])
            ->add('streetNumber', TextType::class, [
                'required' => true,
                'trim' => true,
                'label' => 'Street number'
            ])
            ->add('zip', TextType::class, [
                'required' => true,
                'trim' => true,
                'label' => 'ZIP'
            ])
            ->add('city', TextType::class, [
                'required' => true,
                'trim' => true,
                'label' => 'City'
            ])
            ->add('phone', TelType::class, [
                'required' => true,
                'trim' => true,
                'label' => 'Phone'
            ])
            ->add('customerSubmit', SubmitType::class, ['label' => 'Send Form'])
            ->setAction($this->router->generate(self::CONFIRM_ORDER_ACTION));
    }
}