<?php


namespace App\Application\DTOs;


class AddExtraAjaxDTO implements ApplicationDTOInterface
{
    private int $idOrderedPizza;
    private array $pizzaExtrasId;

    /**
     * @return int
     */
    public function getIdOrderedPizza(): int
    {
        return $this->idOrderedPizza;
    }

    /**
     * @param int $idOrderedPizza
     */
    public function setIdOrderedPizza(int $idOrderedPizza): void
    {
        $this->idOrderedPizza = $idOrderedPizza;
    }

    /**
     * @return array
     */
    public function getPizzaExtrasId(): array
    {
        return $this->pizzaExtrasId;
    }

    /**
     * @param array $pizzaExtrasId
     */
    public function setPizzaExtrasId(array $pizzaExtrasId): void
    {
        $this->pizzaExtrasId = $pizzaExtrasId;
    }
}