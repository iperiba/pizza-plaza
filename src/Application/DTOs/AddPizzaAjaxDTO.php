<?php


namespace App\Application\DTOs;


class AddPizzaAjaxDTO implements ApplicationDTOInterface
{
    private int $idPizza;
    private string $idOrder;
    private int $numberPizzas;
    private float $priceAmount;

    /**
     * @return int
     */
    public function getIdPizza(): int
    {
        return $this->idPizza;
    }

    /**
     * @param int $idPizza
     */
    public function setIdPizza(int $idPizza): void
    {
        $this->idPizza = $idPizza;
    }

    /**
     * @return string
     */
    public function getIdOrder(): string
    {
        return $this->idOrder;
    }

    /**
     * @param string $idOrder
     */
    public function setIdOrder(string $idOrder): void
    {
        $this->idOrder = $idOrder;
    }

    /**
     * @return int
     */
    public function getNumberPizzas(): int
    {
        return $this->numberPizzas;
    }

    /**
     * @param int $numberPizzas
     */
    public function setNumberPizzas(int $numberPizzas): void
    {
        $this->numberPizzas = $numberPizzas;
    }

    /**
     * @return float
     */
    public function getPriceAmount(): float
    {
        return $this->priceAmount;
    }

    /**
     * @param float $priceAmount
     */
    public function setPriceAmount(float $priceAmount): void
    {
        $this->priceAmount = $priceAmount;
    }
}