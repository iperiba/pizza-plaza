<?php


namespace App\Application\Factory;

use App\Application\Repositories\OrderRepository;
use App\Domain\Entities\Order;
use App\Infrastructure\Services\InfrastructureServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class OrderFactory implements ApplicationFactoryinterface
{
    const ID_ORDER_VARIABLE = 'idOrder';

    private $logger;
    private $uuidGenerator;
    private $entityManager;
    private $orderRepository;
    private $requestStack;

    public function __construct(LoggerInterface $logger,
                                InfrastructureServiceInterface $uuidGenerator,
                                EntityManagerInterface $entityManager,
                                OrderRepository $orderRepository,
                                RequestStack $requestStack)

    {
        $this->logger = $logger;
        $this->uuidGenerator = $uuidGenerator;
        $this->entityManager = $entityManager;
        $this->orderRepository = $orderRepository;
        $this->requestStack = $requestStack;
    }

    public function generateOrder(): ?Order
    {
        $session = $this->requestStack->getSession();

        if ($session->has(self::ID_ORDER_VARIABLE)) {
            $idOrder = $session->get(self::ID_ORDER_VARIABLE);
            return $this->orderRepository->getOrderById($idOrder);
        }

        $uuid = $this->uuidGenerator->generateUuid();
        $order = new Order($uuid);
        $this->entityManager->persist($order);
        $this->entityManager->flush();
        $session->set(self::ID_ORDER_VARIABLE, $uuid);

        return $order;
    }
}