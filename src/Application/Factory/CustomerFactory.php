<?php


namespace App\Application\Factory;

use App\Domain\Entities\Customer;
use App\Domain\ValueObjects\Address;
use App\Domain\ValueObjects\Phone;
use Psr\Log\LoggerInterface;

class CustomerFactory implements ApplicationFactoryinterface
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function generateCustomer($builder): ?Customer
    {
        $newCustomer = new Customer();
        $newAddress = new Address($builder->getStreet(), $builder->getStreetNumber(), $builder->getZip(), $builder->getCity());
        $newPhone = new Phone($builder->getPhone());
        $newCustomer->setFirstName($builder->getFirstName());
        $newCustomer->setLastName($builder->getLastName());
        $newCustomer->setAddress($newAddress);
        $newCustomer->setPhone($newPhone);

        return $newCustomer;
    }
}