<?php


namespace App\Application\Services;

use App\Application\Factory\ApplicationFactoryinterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ExtrasBlockAjaxGenerator extends AbstractController implements ApplicationServiceInterface
{
    const TEMPLATE_EJAX_EXTRAS = 'extra_options.html.twig';

    private $logger;
    private $orderFactory;
    private $extraFormsGenerator;
    private $removeOrderedPizzaFormsGenerator;


    public function __construct(LoggerInterface $logger,
                                ApplicationFactoryInterface $orderFactory,
                                ApplicationServiceInterface $extraFormsGenerator,
                                ApplicationServiceInterface $removeOrderedPizzaFormsGenerator)
    {
        $this->logger = $logger;
        $this->orderFactory = $orderFactory;
        $this->extraFormsGenerator = $extraFormsGenerator;
        $this->removeOrderedPizzaFormsGenerator = $removeOrderedPizzaFormsGenerator;
    }

    public function generateAjaxResponse()
    {
        $order = $this->orderFactory->generateOrder();
        $orderedPizzasArray = (!empty($result = $order->getOrderedPizzas()->toArray())) ? $result : array();
        $extrasForms = (!empty($orderedPizzasArray)) ? $this->extraFormsGenerator->getAllExtraForms($orderedPizzasArray) : array();
        $removeOrderedPizzasForms = (!empty($orderedPizzasArray)) ? $this->removeOrderedPizzaFormsGenerator->getAllRemoveOrderedPizzaForms($orderedPizzasArray) : array();

        $renderArray = array_merge(
            array('orderedPizzasArray' => $orderedPizzasArray),
            $extrasForms,
            $removeOrderedPizzasForms
        );

        return $this->render(self::TEMPLATE_EJAX_EXTRAS, $renderArray)->getContent();
    }
}