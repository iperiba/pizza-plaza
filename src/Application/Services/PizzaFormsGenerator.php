<?php


namespace App\Application\Services;


use App\Application\Forms\PizzaSelectionForm;
use App\Domain\Services\DomainServiceInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormFactoryInterface;


class PizzaFormsGenerator extends AbstractType implements ApplicationServiceInterface
{
    const PREFIX_PIZZA_FORM = 'pizzaForm';

    private $logger;
    private $pizzaArrayOperator;
    private $formFactory;

    public function __construct(LoggerInterface $logger, DomainServiceInterface $pizzaArrayOperator, FormFactoryInterface $formFactory)
    {
        $this->logger = $logger;
        $this->pizzaArrayOperator = $pizzaArrayOperator;
        $this->formFactory = $formFactory;
    }

    public function getAllPizzaForms(array $pizzaArray): array
    {
        $idPizzaArray = $this->pizzaArrayOperator->getArrayPizzaIds($pizzaArray);
        $pizzaFormArray = array();

        foreach ($pizzaArray as $pizza) {
            $idPizza = $pizza->getId();
            $numberPizzas = 0;
            $idOrder = 0;
            $pizzaFormId = self::PREFIX_PIZZA_FORM . $idPizza;
            $priceAmount = $pizza->getPrice()->getAmount();

            $pizzaForm = $this->formFactory->create(PizzaSelectionForm::class,
                [
                    'idPizza' => $idPizza,
                    'numberPizzas' => $numberPizzas,
                    'idOrder' => $idOrder,
                    'priceAmount' => $priceAmount
                ],
                [
                    'attr' => array(
                        'id' => $pizzaFormId,
                        'class' => $pizzaFormId
                    )
                ]);

            $pizzaFormArray[$pizzaFormId] = $pizzaForm->createView();
        }

        return $pizzaFormArray;
    }
}