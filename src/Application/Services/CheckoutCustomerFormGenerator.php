<?php


namespace App\Application\Services;

use App\Application\Forms\CheckoutCustomerForm;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormFactoryInterface;

class CheckoutCustomerFormGenerator extends AbstractType implements ApplicationServiceInterface
{
    const PREFIX_EXTRA_FORM = 'customerForm';

    private $logger;
    private $formFactory;

    public function __construct(LoggerInterface $logger, FormFactoryInterface $formFactory)
    {
        $this->logger = $logger;
        $this->formFactory = $formFactory;
    }

    public function getCustomerForm(string $idOrder)
    {
        $extraFormId = self::PREFIX_EXTRA_FORM . $idOrder;
        $checkoutCustomerForm = $this->formFactory->create(CheckoutCustomerForm::class,
            [
                'idOrder' => $idOrder,
            ],
            [
                'attr' => array(
                    'id' => $extraFormId,
                    'class' => self::PREFIX_EXTRA_FORM
                )
            ]);

        return $checkoutCustomerForm->createView();
    }
}