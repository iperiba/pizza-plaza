<?php


namespace App\Application\Services;

use App\Application\Forms\RemoveOrderedPizzaForm;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormFactoryInterface;

class RemoveOrderedPizzaFormsGenerator extends AbstractType implements ApplicationServiceInterface
{
    const PREFIX_EXTRA_FORM = 'removeOrderedPizzaForm';

    private $logger;
    private $formFactory;

    public function __construct(LoggerInterface $logger, FormFactoryInterface $formFactory)
    {
        $this->logger = $logger;
        $this->formFactory = $formFactory;
    }

    public function getAllRemoveOrderedPizzaForms(array $orderedPizzasArray): array
    {
        $removedOrderedPizzaFormArray = array();

        foreach ($orderedPizzasArray as $orderedPizza) {
            $removeOrderedPizzaFormId = self::PREFIX_EXTRA_FORM . $orderedPizza->getId();

            $removeOrderedPizzaForm = $this->formFactory->create(RemoveOrderedPizzaForm::class,
                [
                    'idOrderedPizza' => $orderedPizza->getId(),
                ],
                [
                    'attr' => array(
                        'id' => $removeOrderedPizzaFormId,
                        'class' => $removeOrderedPizzaFormId
                    )
                ]);

            $removedOrderedPizzaFormArray[$removeOrderedPizzaFormId] = $removeOrderedPizzaForm->createView();
        }

        return $removedOrderedPizzaFormArray;
    }
}