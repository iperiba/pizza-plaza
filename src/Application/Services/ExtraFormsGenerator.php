<?php


namespace App\Application\Services;

use App\Application\Forms\ExtraSelectionForm;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormFactoryInterface;

class ExtraFormsGenerator extends AbstractType implements ApplicationServiceInterface
{
    const PREFIX_EXTRA_FORM = 'extraForm';

    private $logger;
    private $formFactory;

    public function __construct(LoggerInterface $logger, FormFactoryInterface $formFactory)
    {
        $this->logger = $logger;
        $this->formFactory = $formFactory;
    }

    public function getAllExtraForms(array $orderedPizzasArray): array
    {
        $extrasFormArray = array();

        foreach ($orderedPizzasArray as $orderedPizza) {

            $pizzaExtras = $orderedPizza->getPizza()->getExtras()->toArray();

            if (empty($pizzaExtras)) {
                continue;
            }

            $extrasAlreadySelected = $orderedPizza->getExtras()->toArray();
            $extraFormId = self::PREFIX_EXTRA_FORM . $orderedPizza->getId();

            $extraForm = $this->formFactory->create(ExtraSelectionForm::class,
                [
                    'idOrderedPizza' => $orderedPizza->getId(),
                    'pizzaExtras' => $pizzaExtras,
                    'extrasAlreadySelected' => $extrasAlreadySelected,
                ],
                [
                    'attr' => array(
                        'id' => $extraFormId,
                        'class' => $extraFormId
                    )
                ]);

            $extrasFormArray[$extraFormId] = $extraForm->createView();
        }

        return $extrasFormArray;
    }
}