<?php


namespace App\Application\Services;

use App\Application\Factory\ApplicationFactoryinterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class ReceiptBlockAjaxGenerator extends AbstractController implements ApplicationServiceInterface
{
    const TEMPLATE_EJAX_EXTRAS = 'receipt.html.twig';

    private $logger;
    private $orderFactory;
    private $router;
    private $requestStack;


    public function __construct(LoggerInterface $logger,
                                ApplicationFactoryInterface $orderFactory,
                                RouterInterface $router,
                                RequestStack $requestStack)
    {
        $this->logger = $logger;
        $this->orderFactory = $orderFactory;
        $this->router = $router;
        $this->requestStack = $requestStack;
    }

    public function generateAjaxResponse()
    {
        $order = $this->orderFactory->generateOrder();
        $pathUri = $this->requestStack->getCurrentRequest()->getRequestUri();

        $renderArray = array_merge(
            array('order' => $order),
            array('checkoutRoute' => $this->router->getRouteCollection()->get('checkout')->getPath()),
            array('pathUri' => $pathUri)
        );

        return $this->render(self::TEMPLATE_EJAX_EXTRAS, $renderArray)->getContent();
    }
}