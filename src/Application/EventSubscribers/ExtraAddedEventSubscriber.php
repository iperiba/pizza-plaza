<?php


namespace App\Application\EventSubscribers;

use App\Application\Events\ApplicationEventAbstractClass;
use App\Application\Events\ExtraAddedEvent;
use App\Application\Factory\ApplicationFactoryinterface;
use App\Application\Repositories\CustomRepositoryInterface;
use App\Application\Services\ApplicationServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ExtraAddedEventSubscriber extends AbstractController implements EventSubscriberInterface
{
    private $logger;
    private $entityManager;
    private $orderedPizzaRepository;
    private $extraRepository;
    private $orderFactory;
    private $extrasBlockAjaxGenerator;
    private $receiptBlockAjaxGenerator;


    public function __construct(LoggerInterface $logger,
                                EntityManagerInterface $entityManager,
                                CustomRepositoryInterface $orderedPizzaRepository,
                                CustomRepositoryInterface $extraRepository,
                                ApplicationFactoryinterface $orderFactory,
                                ApplicationServiceInterface $extrasBlockAjaxGenerator,
                                ApplicationServiceInterface $receiptBlockAjaxGenerator)
    {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->orderedPizzaRepository = $orderedPizzaRepository;
        $this->extraRepository = $extraRepository;
        $this->orderFactory = $orderFactory;
        $this->extrasBlockAjaxGenerator = $extrasBlockAjaxGenerator;
        $this->receiptBlockAjaxGenerator = $receiptBlockAjaxGenerator;
    }

    public static function getSubscribedEvents()
    {
        return [
            ExtraAddedEvent::NAME => [
                ['addExtras', 0],
                ['generateAjaxResponse', -10],
            ],
        ];
    }

    public function addExtras(ApplicationEventAbstractClass $event)
    {
        $order = $this->orderFactory->generateOrder();
        $addExtraAjaxDTO = $event->getAddExtraAjaxDTO();
        $idOrderedPizza = $addExtraAjaxDTO->getIdOrderedPizza();
        $extrasIdArray = $addExtraAjaxDTO->getPizzaExtrasId();

        $orderedPizza = $this->orderedPizzaRepository->getOrderedPizzaById($idOrderedPizza);
        $alreadyAddedExtras = $orderedPizza->getExtras()->toArray();

        if (count($alreadyAddedExtras) > 0) {
            foreach ($alreadyAddedExtras as $alreadyAddedExtra) {
                $orderedPizza->removeExtra($alreadyAddedExtra);
                $order->removeFromTotalPrice($alreadyAddedExtra->getPrice()->getAmount());
            }
        }

        $extraEntitiesArray = $this->generateExtraEntitiesArray($extrasIdArray);

        foreach ($extraEntitiesArray as $extraEntity) {
            $orderedPizza->addExtra($extraEntity);
            $order->addToTotalPrice($extraEntity->getPrice()->getAmount());
        }

        $this->entityManager->persist($orderedPizza);
        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }

    public function generateAjaxResponse(ApplicationEventAbstractClass $event)
    {
        $event->setExtraForms($this->extrasBlockAjaxGenerator->generateAjaxResponse());
        $event->setReceipt($this->receiptBlockAjaxGenerator->generateAjaxResponse());
    }

    private function generateExtraEntitiesArray(array $extrasIdArray)
    {
        $extraEntitiesArray = array();

        foreach ($extrasIdArray as $extraId) {
            $extraEntity = $this->extraRepository->getExtraById($extraId);
            array_push($extraEntitiesArray, $extraEntity);
        }

        return $extraEntitiesArray;
    }
}