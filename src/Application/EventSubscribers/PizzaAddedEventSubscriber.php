<?php


namespace App\Application\EventSubscribers;


use App\Application\DTOs\ApplicationDTOInterface;
use App\Application\Events\ApplicationEventAbstractClass;
use App\Application\Events\PizzaAddedEvent;
use App\Application\Factory\ApplicationFactoryinterface;
use App\Application\Repositories\CustomRepositoryInterface;
use App\Application\Services\ApplicationServiceInterface;
use App\Domain\Entities\Order;
use App\Domain\Entities\OrderedPizza;
use App\Domain\Entities\Pizza;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PizzaAddedEventSubscriber extends AbstractController implements EventSubscriberInterface
{
    private $logger;
    private $entityManager;
    private $pizzaRepository;
    private $orderFactory;
    private $extrasBlockAjaxGenerator;
    private $receiptBlockAjaxGenerator;


    public function __construct(LoggerInterface $logger,
                                EntityManagerInterface $entityManager,
                                CustomRepositoryInterface $pizzaRepository,
                                ApplicationFactoryinterface $orderFactory,
                                ApplicationServiceInterface $extrasBlockAjaxGenerator,
                                ApplicationServiceInterface $receiptBlockAjaxGenerator)
    {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->pizzaRepository = $pizzaRepository;
        $this->orderFactory = $orderFactory;
        $this->extrasBlockAjaxGenerator = $extrasBlockAjaxGenerator;
        $this->receiptBlockAjaxGenerator = $receiptBlockAjaxGenerator;
    }

    public static function getSubscribedEvents()
    {
        // return the subscribed events, their methods and priorities
        return [
            PizzaAddedEvent::NAME => [
                ['processPizzas', 0],
                ['generateAjaxResponse', -10],
            ],
        ];
    }

    public function processPizzas(ApplicationEventAbstractClass $event)
    {
        $order = $this->orderFactory->generateOrder();
        $addPizzaAjaxDTO = $event->getAddPizzaAjaxDTO();
        $idPizza = $addPizzaAjaxDTO->getIdPizza();
        $pizzaEntity = $this->pizzaRepository->getPizzaById($idPizza);
        $criteria = Criteria::create()->where(Criteria::expr()->eq("pizza", $pizzaEntity));
        $orderedPizzasAlreadyPersisted = $order->getOrderedPizzas()->matching($criteria);

        if (count($orderedPizzasAlreadyPersisted) > 0) {
            foreach ($orderedPizzasAlreadyPersisted as $orderedPizzaAlreadyPersisted) {
                $orderedPizzaAlreadyPersisted->setOrder(null);
                $order->getOrderedPizzas()->removeElement($orderedPizzaAlreadyPersisted);
                $order->removeFromTotalPrice($pizzaEntity->getPrice()->getAmount());

                $extrasOrderedPizza = $orderedPizzaAlreadyPersisted->getExtras()->toArray();
                foreach ($extrasOrderedPizza as $extraOrderedPizza) {
                    $orderedPizzaAlreadyPersisted->removeExtra($extraOrderedPizza);
                    $order->removeFromTotalPrice($extraOrderedPizza->getPrice()->getAmount());
                }

                $this->entityManager->persist($orderedPizzaAlreadyPersisted);
            }
        }

        if ($addPizzaAjaxDTO->getNumberPizzas() === 0) {
            return;
        }

        $arrayOrderedPizzas = $this->generateOrderedPizzas($order, $addPizzaAjaxDTO, $pizzaEntity);

        foreach ($arrayOrderedPizzas as $orderedPizza) {
            $order->addOrderedPizza($orderedPizza);
            $this->entityManager->persist($orderedPizza);
            $order->addToTotalPrice($pizzaEntity->getPrice()->getAmount());
        }

        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }

    public function generateAjaxResponse(ApplicationEventAbstractClass $event)
    {
        $event->setExtraForms($this->extrasBlockAjaxGenerator->generateAjaxResponse());
        $event->setReceipt($this->receiptBlockAjaxGenerator->generateAjaxResponse());
    }

    private function generateOrderedPizzas(Order $order, ApplicationDTOInterface $addPizzaAjaxDTO, Pizza $pizzaEntity): array
    {
        $arrayOrderedPizzas = array();
        $numberPizzas = $addPizzaAjaxDTO->getNumberPizzas();

        for ($i = 0; $i < $numberPizzas; $i++) {
            $orderedPizza = new OrderedPizza();
            $orderedPizza->setPizza($pizzaEntity);
            $orderedPizza->setOrder($order);
            $arrayOrderedPizzas[$i] = $orderedPizza;
        }

        return $arrayOrderedPizzas;
    }
}