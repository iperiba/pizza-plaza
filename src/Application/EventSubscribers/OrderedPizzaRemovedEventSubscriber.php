<?php


namespace App\Application\EventSubscribers;

use App\Application\Events\ApplicationEventAbstractClass;
use App\Application\Events\OrderedPizzaRemoved;
use App\Application\Factory\ApplicationFactoryinterface;
use App\Application\Repositories\CustomRepositoryInterface;
use App\Application\Services\ApplicationServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderedPizzaRemovedEventSubscriber extends AbstractController implements EventSubscriberInterface
{

    private $logger;
    private $entityManager;
    private $orderedPizzaRepository;
    private $orderFactory;
    private $extrasBlockAjaxGenerator;
    private $receiptBlockAjaxGenerator;


    public function __construct(LoggerInterface $logger,
                                EntityManagerInterface $entityManager,
                                CustomRepositoryInterface $orderedPizzaRepository,
                                ApplicationFactoryinterface $orderFactory,
                                ApplicationServiceInterface $extrasBlockAjaxGenerator,
                                ApplicationServiceInterface $receiptBlockAjaxGenerator)
    {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->orderedPizzaRepository = $orderedPizzaRepository;
        $this->orderFactory = $orderFactory;
        $this->extrasBlockAjaxGenerator = $extrasBlockAjaxGenerator;
        $this->receiptBlockAjaxGenerator = $receiptBlockAjaxGenerator;
    }

    public static function getSubscribedEvents()
    {
        return [
            OrderedPizzaRemoved::NAME => [
                ['removeOrderedPizza', 0],
                ['generateAjaxResponse', -10],
            ],
        ];
    }

    public function removeOrderedPizza(ApplicationEventAbstractClass $event)
    {
        $order = $this->orderFactory->generateOrder();
        $orderedPizzaId = $event->getOrderedPizzaId();
        $orderedPizza = $this->orderedPizzaRepository->getOrderedPizzaById($orderedPizzaId);

        $orderedPizza->setOrder(null);
        $order->getOrderedPizzas()->removeElement($orderedPizza);
        $order->removeFromTotalPrice($orderedPizza->getPizza()->getPrice()->getAmount());

        $extrasOrderedPizza = $orderedPizza->getExtras()->toArray();

        foreach ($extrasOrderedPizza as $extraOrderedPizza) {
            $orderedPizza->removeExtra($extraOrderedPizza);
            $order->removeFromTotalPrice($extraOrderedPizza->getPrice()->getAmount());
        }

        $this->entityManager->persist($orderedPizza);
        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }

    public function generateAjaxResponse(ApplicationEventAbstractClass $event)
    {
        $event->setExtraForms($this->extrasBlockAjaxGenerator->generateAjaxResponse());
        $event->setReceipt($this->receiptBlockAjaxGenerator->generateAjaxResponse());
    }
}