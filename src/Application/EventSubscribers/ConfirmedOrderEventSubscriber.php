<?php


namespace App\Application\EventSubscribers;


use App\Application\Events\ApplicationEventAbstractClass;
use App\Application\Events\OrderConfirmed;
use App\Application\Factory\ApplicationFactoryinterface;
use App\Application\Repositories\CustomRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ConfirmedOrderEventSubscriber extends AbstractController implements EventSubscriberInterface
{
    const ID_ORDER_VARIABLE = 'idOrder';

    private $logger;
    private $entityManager;
    private $orderFactory;
    private $customerFactory;
    private $orderRepository;
    private $requestStack;


    public function __construct(LoggerInterface $logger,
                                EntityManagerInterface $entityManager,
                                ApplicationFactoryinterface $orderFactory,
                                ApplicationFactoryinterface $customerFactory,
                                CustomRepositoryInterface $orderRepository,
                                RequestStack $requestStack)
    {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->orderFactory = $orderFactory;
        $this->customerFactory = $customerFactory;
        $this->orderRepository = $orderRepository;
        $this->requestStack = $requestStack;
    }

    public static function getSubscribedEvents()
    {
        return [
            OrderConfirmed::NAME => [
                ['persistOrder', 0],
                ['cleanSession', -10],
            ],
        ];
    }

    public function persistOrder(ApplicationEventAbstractClass $event)
    {
        $newCustomer = $this->customerFactory->generateCustomer($event->getOrderConfirmedDTO());

        if (empty($newCustomer)) {
            throw new \Exception(sprintf("Customer couldnot be created"));
        }

        $orderId = $event->getOrderConfirmedDTO()->getIdOrder();
        $order = $this->orderRepository->getOrderById($event->getOrderConfirmedDTO()->getIdOrder());

        if (empty($order)) {
            throw new \Exception(sprintf("The order %s could not be retrieved", $orderId));
        }

        $order->setCustomer($newCustomer);
        $order->setFinishedAt(new \Datetime());
        $newCustomer->addNewRelatedOrder($order);

        $this->entityManager->persist($order);
        $this->entityManager->persist($newCustomer);

        $this->entityManager->flush();

        $event->setPersistedCustomer($newCustomer);
        $event->setPersistedOrder($order);
    }

    public function cleanSession(ApplicationEventAbstractClass $event)
    {
        $session = $this->requestStack->getSession();
        if ($session->has(self::ID_ORDER_VARIABLE)) {
            $session->remove(self::ID_ORDER_VARIABLE);
        }
    }
}