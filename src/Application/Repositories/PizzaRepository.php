<?php


namespace App\Application\Repositories;


use App\Domain\Entities\Pizza;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PizzaRepository extends ServiceEntityRepository implements CustomRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pizza::class);
    }

    public function getAllPizzas(): array
    {
        return $this->findAll();
    }

    public function getPizzaById(int $id): ?Pizza
    {
        return $this->find($id);
    }

}