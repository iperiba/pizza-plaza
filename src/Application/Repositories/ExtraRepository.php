<?php

namespace App\Application\Repositories;

use App\Domain\Entities\Extra;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;

class ExtraRepository extends ServiceEntityRepository implements CustomRepositoryInterface
{
    private $logger;

    public function __construct(ManagerRegistry $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, Extra::class);
        $this->logger = $logger;
    }

    public function getAllExtras(): array
    {
        return $this->findAll();
    }

    public function getExtraById(string $id): ?Extra
    {
        return $this->find($id);
    }
}