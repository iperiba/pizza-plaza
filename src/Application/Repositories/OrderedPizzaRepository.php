<?php


namespace App\Application\Repositories;

use App\Domain\Entities\OrderedPizza;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class OrderedPizzaRepository extends ServiceEntityRepository implements CustomRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderedPizza::class);
    }

    public function getOrderedPizzaById(string $id): ?OrderedPizza
    {
        return $this->find($id);
    }
}