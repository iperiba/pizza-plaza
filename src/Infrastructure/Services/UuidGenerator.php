<?php


namespace App\Infrastructure\Services;

use Symfony\Component\Uid\Uuid;

class UuidGenerator implements InfrastructureServiceInterface
{
    public function generateUuid()
    {
        return Uuid::v4();
    }
}