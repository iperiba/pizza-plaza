<?php


namespace App\Infrastructure\Services;

use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\RouterInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class BreadcrumbBuilder implements InfrastructureServiceInterface
{
    const HOME_EXTERNAL_NAME = "Home";

    public $breadcrumbGenerator;
    public $logger;
    public $router;
    public $baseUrl;

    public function __construct(Breadcrumbs $breadcrumbGenerator, LoggerInterface $logger, RouterInterface $router, string $baseUrl)
    {
        $this->breadcrumbGenerator = $breadcrumbGenerator;
        $this->logger = $logger;
        $this->router = $router;
        $this->baseUrl = $baseUrl;
    }

    public function breadCrumbwithPrependHome(array $breadcrumbArray) {

        $this->breadcrumbGenerator->prependItem(self::HOME_EXTERNAL_NAME, $this->baseUrl);

        if (empty($breadcrumbArray)) {
            return $this->breadcrumbGenerator;
        }

        foreach($breadcrumbArray as $index => $value) {
            try {
                $realObjectRoute = $this->router->generate($value);
                $this->breadcrumbGenerator->addItem($index, $realObjectRoute);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }

        return $this->breadcrumbGenerator;
    }
}