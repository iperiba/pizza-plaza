<?php

namespace App\Infrastructure\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LegacyBridge implements InfrastructureServiceInterface

{
    const AVAILABLE_SITES_LEGACY = ['main', 'about', 'imprint', 'contact'];
    const CURRENT_SITE = "main";
    const SITE_PARAMETER_NAME = "site";
    const GET_METHOD = "get";
    const LEGACY_INDEX_FOLDER = "../pages/index.php";

    public static function handleLegacyRequest(Request $request, Response $response, string $publicDirectory): ?string
    {
        $siteParameter = $request->query->get(self::SITE_PARAMETER_NAME);
        $currentSite = self::CURRENT_SITE;

        // Load requested site-GET parameter
        if ($request->isMethod(self::GET_METHOD) && isset($siteParameter) && in_array($siteParameter, self::AVAILABLE_SITES_LEGACY)) {
            $currentSite = $_GET[self::SITE_PARAMETER_NAME];
        }

        // Possibly (re-)set some env vars (e.g. to handle forms
        // posting to PHP_SELF):
        $p = $request->getPathInfo();
        $_SERVER['PHP_SELF'] = $p;
        $_SERVER['SCRIPT_NAME'] = $p;
        $_SERVER['SCRIPT_FILENAME'] = $_ENV['LEGACY_INDEX_FOLDER'];

        require self::LEGACY_INDEX_FOLDER;

        exit;
    }
}

