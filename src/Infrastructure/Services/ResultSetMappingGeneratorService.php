<?php


namespace App\Infrastructure\Services;


use Doctrine\ORM\Query\ResultSetMapping;

class ResultSetMappingGeneratorService implements InfrastructureServiceInterface
{
    public function generateResultSetMapping()
    {
        return new ResultSetMapping();
    }

}