<?php


namespace App\Infrastructure\Exceptions;


class NoCheckOutCustomerFormParameterArrayFound extends \Exception implements InfrastructureExceptionInterface
{
    const MESSAGE = "No checkout customer form parameter found";

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}