<?php

namespace App\Infrastructure\Exceptions;

class NonExistantSiteParameterException extends \Exception implements InfrastructureExceptionInterface
{
    const MESSAGE = "The request doesn't contain any site parameter";

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}