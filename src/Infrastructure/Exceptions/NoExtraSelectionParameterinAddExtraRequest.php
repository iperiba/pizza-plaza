<?php


namespace App\Infrastructure\Exceptions;


class NoExtraSelectionParameterinAddExtraRequest extends \Exception implements InfrastructureExceptionInterface
{
    const MESSAGE = "No extra selection array in add extra request";

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}