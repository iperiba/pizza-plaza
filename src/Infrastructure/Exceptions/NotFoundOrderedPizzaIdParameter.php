<?php

namespace App\Infrastructure\Exceptions;

class NotFoundOrderedPizzaIdParameter extends \Exception implements InfrastructureExceptionInterface
{
    const MESSAGE = "Orderes pizza id parameter not found";

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}