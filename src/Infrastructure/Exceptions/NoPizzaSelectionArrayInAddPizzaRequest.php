<?php


namespace App\Infrastructure\Exceptions;


class NoPizzaSelectionArrayInAddPizzaRequest extends \Exception implements InfrastructureExceptionInterface
{
    const MESSAGE = "No pizza selection array in add pizza request";

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}