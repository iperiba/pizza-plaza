<?php

namespace App\Infrastructure\Exceptions;

class LegacyIndexFolderParameterException extends \Exception implements InfrastructureExceptionInterface
{
    const MESSAGE = "There is not a legacy index folder parameter in .env file";

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}