<?php


namespace App\Infrastructure\Controllers;

use App\Application\DTOs\ApplicationDTOInterface;
use App\Application\DTOs\OrderConfirmedDTO;
use App\Application\Events\OrderConfirmed;
use App\Infrastructure\Exceptions\NoCheckOutCustomerFormParameterArrayFound;
use App\Infrastructure\Services\BreadcrumbBuilder;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

class ConfirmOrderController extends AbstractController
{
    const FINAL_PAGE_TEAMPLATE = 'final_page.html.twig';
    const ARRAY_PARAMETER_CUSTOMER_FORM_NAME = 'checkout_customer_form';

    public function confirmOrder(Request $request, EventDispatcherInterface $eventDispatcher, LoggerInterface $logger, BreadcrumbBuilder $breadcrumbBuilder)
    {

        if (!$request->request->has(self::ARRAY_PARAMETER_CUSTOMER_FORM_NAME)) {
            throw new NoCheckOutCustomerFormParameterArrayFound();
        }

        $pathUri = $request->getRequestUri();
        $checkoutCustomerFormArray = $request->request->get(self::ARRAY_PARAMETER_CUSTOMER_FORM_NAME);
        $orderConfirmedDTO = $this->populateOrderConfirmedDTO($checkoutCustomerFormArray);
        $event = new OrderConfirmed($orderConfirmedDTO);
        try {
            $responseEventDispatch = $eventDispatcher->dispatch($event, OrderConfirmed::NAME);
        } catch (\Exception $e) {
            $logger->error($e->getMessage());
            $renderArray = array_merge(
                array('message' => "There was an error while processing your order"),
                array('pathUri' => $pathUri),
                array('success' => false)
            );

            return $this->render(self::FINAL_PAGE_TEAMPLATE, $renderArray);
        }

        $persistedCustomer = $responseEventDispatch->getPersistedCustomer();
        $persistedOrder = $responseEventDispatch->getPersistedOrder();

        $breadcrumbArray = array("Online ordering"=>"online_ordering", "Confirmation page"=>"confirm_order");
        $breadcrumbBuilder->breadCrumbwithPrependHome($breadcrumbArray);

        $renderArray = array_merge(
            array('message' => "Order successfully persisted"),
            array('order' => $persistedOrder),
            array('customer' => $persistedCustomer),
            array('success' => true)
        );

        return $this->render(self::FINAL_PAGE_TEAMPLATE, $renderArray);
    }

    private function populateOrderConfirmedDTO(array $checkoutCustomerFormArray): ApplicationDTOInterface
    {
        $newOrderConfirmedDTO = new OrderConfirmedDTO();
        $newOrderConfirmedDTO->setIdOrder($checkoutCustomerFormArray["idOrder"]);
        $newOrderConfirmedDTO->setFirstName((!empty($result = $checkoutCustomerFormArray["firstName"])) ? $result : "");
        $newOrderConfirmedDTO->setLastName((!empty($result = $checkoutCustomerFormArray["lastName"])) ? $result : "");
        $newOrderConfirmedDTO->setStreet((!empty($result = $checkoutCustomerFormArray["street"])) ? $result : "");
        $newOrderConfirmedDTO->setStreetNumber((!empty($result = $checkoutCustomerFormArray["streetNumber"])) ? $result : "");
        $newOrderConfirmedDTO->setZip((!empty($result = $checkoutCustomerFormArray["zip"])) ? $result : "");
        $newOrderConfirmedDTO->setCity((!empty($result = $checkoutCustomerFormArray["city"])) ? $result : "");
        $newOrderConfirmedDTO->setPhone((!empty($result = $checkoutCustomerFormArray["phone"])) ? $result : "");

        return $newOrderConfirmedDTO;
    }
}