<?php


namespace App\Infrastructure\Controllers;

use App\Application\DTOs\AddExtraAjaxDTO;
use App\Application\DTOs\ApplicationDTOInterface;
use App\Application\Events\ExtraAddedEvent;
use App\Infrastructure\Exceptions\NoExtraSelectionParameterinAddExtraRequest;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AddExtraController extends AbstractController
{
    const PIZZA_EXTRAS_PARAMETER = 'pizzaExtrasId';
    const ORDERED_PIZZA_ID_PARAMETER = 'idOrderedPizza';

    public function addExtra(Request $request, EventDispatcherInterface $eventDispatcher)
    {
        if (!$request->request->has(self::PIZZA_EXTRAS_PARAMETER)
            || !$request->request->has(self::ORDERED_PIZZA_ID_PARAMETER)) {
            throw new NoExtraSelectionParameterinAddExtraRequest();
        }

        $pizzaExtrasId = $request->request->get(self::PIZZA_EXTRAS_PARAMETER);
        $idOrderedPizza = $request->request->get(self::ORDERED_PIZZA_ID_PARAMETER);

        $addExtraAjaxDTO = $this->populateAddExtraAjaxDTO($idOrderedPizza, $pizzaExtrasId);
        $event = new ExtraAddedEvent($addExtraAjaxDTO);
        $responseEventDispatch = $eventDispatcher->dispatch($event, ExtraAddedEvent::NAME);

        $responseArray["extraFormsHtml"] = $responseEventDispatch->getExtraForms();
        $responseArray["receiptHtml"] = $responseEventDispatch->getReceipt();

        return new Response(json_encode($responseArray));
    }

    private function populateAddExtraAjaxDTO(int $idOrderedPizza, array $pizzaExtrasId): ApplicationDTOInterface
    {
        $addExtraAjaxDTO = new AddExtraAjaxDTO();
        $addExtraAjaxDTO->setIdOrderedPizza(intval($idOrderedPizza));
        $addExtraAjaxDTO->setPizzaExtrasId($pizzaExtrasId);

        return $addExtraAjaxDTO;
    }
}