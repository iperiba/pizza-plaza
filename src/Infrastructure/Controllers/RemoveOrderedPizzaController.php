<?php


namespace App\Infrastructure\Controllers;

use App\Application\Events\OrderedPizzaRemoved;
use App\Infrastructure\Exceptions\NotFoundOrderedPizzaIdParameter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class RemoveOrderedPizzaController extends AbstractController
{
    const NAME_ARRAY_POST_PARAMETERS = 'remove_ordered_pizza_form';
    const ORDERED_PIZZA_ID_PARAMETER = "idOrderedPizza";


    public function removeOrderedPizza(Request $request, EventDispatcherInterface $eventDispatcher)
    {
        if (!$request->request->has(self::NAME_ARRAY_POST_PARAMETERS)
            || !array_key_exists(self::ORDERED_PIZZA_ID_PARAMETER, $request->request->get(self::NAME_ARRAY_POST_PARAMETERS))) {
            throw new NotFoundOrderedPizzaIdParameter();
        }

        $orderedPizzaId = $request->request->get(self::NAME_ARRAY_POST_PARAMETERS)[self::ORDERED_PIZZA_ID_PARAMETER];
        $event = new OrderedPizzaRemoved($orderedPizzaId);
        $responseEventDispatch = $eventDispatcher->dispatch($event, OrderedPizzaRemoved::NAME);

        $responseArray["extraFormsHtml"] = $responseEventDispatch->getExtraForms();
        $responseArray["receiptHtml"] = $responseEventDispatch->getReceipt();

        return new Response(json_encode($responseArray));
    }
}