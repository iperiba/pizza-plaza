<?php


namespace App\Infrastructure\Controllers;

use App\Application\Factory\OrderFactory;
use App\Application\Services\CheckoutCustomerFormGenerator;
use App\Infrastructure\Services\BreadcrumbBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

class CheckoutController extends AbstractController
{
    const CHECKOUT_PAGE_TEMPLATE = 'checkout_page.html.twig';

    public function setForm(Request $request,
                            OrderFactory $orderFactory,
                            RouterInterface $router,
                            CheckoutCustomerFormGenerator $checkoutCustomerFormGenerator,
                            BreadcrumbBuilder $breadcrumbBuilder)
    {
        $order = $orderFactory->generateOrder();
        $orderId = $order->getId();
        $checkoutCustomerForm = $checkoutCustomerFormGenerator->getCustomerForm($orderId);
        $baseUri = $request->getSchemeAndHttpHost();
        $pathUri = $request->getRequestUri();

        $breadcrumbArray = array("Online ordering"=>"online_ordering", "Checkout page"=>"checkout");
        $breadcrumbBuilder->breadCrumbwithPrependHome($breadcrumbArray);

        $renderArray = array_merge(
            array('order' => $order),
            array('baseUri' => $baseUri),
            array('pathUri' => $pathUri),
            array('checkoutCustomerForm' => $checkoutCustomerForm),
            array('checkoutRoute' => $router->getRouteCollection()->get('checkout')->getPath())
        );

        return $this->render(self::CHECKOUT_PAGE_TEMPLATE, $renderArray);
    }
}