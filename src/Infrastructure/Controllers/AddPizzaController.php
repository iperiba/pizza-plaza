<?php


namespace App\Infrastructure\Controllers;

use App\Application\DTOs\AddPizzaAjaxDTO;
use App\Application\DTOs\ApplicationDTOInterface;
use App\Application\Events\PizzaAddedEvent;
use App\Infrastructure\Exceptions\NoPizzaSelectionArrayInAddPizzaRequest;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AddPizzaController extends AbstractController
{
    const NAME_ARRAY_POST_PARAMETERS = 'pizza_selection_form';

    public function addPizza(Request $request, EventDispatcherInterface $eventDispatcher)
    {
        if (!$request->request->has(self::NAME_ARRAY_POST_PARAMETERS)) {
            throw new NoPizzaSelectionArrayInAddPizzaRequest();
        }

        $arrayAddPizzaPostParameters = $request->request->get(self::NAME_ARRAY_POST_PARAMETERS);
        $addPizzaAjaxDTO = $this->populateAddPizzaAjaxDTO($arrayAddPizzaPostParameters);
        $event = new PizzaAddedEvent($addPizzaAjaxDTO);
        $responseEventDispatch = $eventDispatcher->dispatch($event, PizzaAddedEvent::NAME);

        $responseArray["extraFormsHtml"] = $responseEventDispatch->getExtraForms();
        $responseArray["receiptHtml"] = $responseEventDispatch->getReceipt();

        return new Response(json_encode($responseArray));
    }

    private function populateAddPizzaAjaxDTO(array $arrayAddPizzaPostParameters): ApplicationDTOInterface
    {
        $addPizzaAjaxDTO = new AddPizzaAjaxDTO();

        if (array_key_exists('idPizza', $arrayAddPizzaPostParameters)) {
            $addPizzaAjaxDTO->setIdPizza(intval($arrayAddPizzaPostParameters['idPizza']));
        }

        if (array_key_exists('idOrder', $arrayAddPizzaPostParameters)) {
            $addPizzaAjaxDTO->setIdOrder($arrayAddPizzaPostParameters['idOrder']);
        }

        if (array_key_exists('priceAmount', $arrayAddPizzaPostParameters)) {
            $addPizzaAjaxDTO->setPriceAmount(floatval($arrayAddPizzaPostParameters['priceAmount']));
        }

        if (array_key_exists('numberPizzas', $arrayAddPizzaPostParameters)) {
            $addPizzaAjaxDTO->setNumberPizzas(intval($arrayAddPizzaPostParameters['numberPizzas']));
        }

        return $addPizzaAjaxDTO;
    }
}