<?php

namespace App\Infrastructure\Controllers;

use App\Domain\Entities\Order;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends AbstractDashboardController
{
    private $em;
    private $paginator;
    private $request;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->paginator = $paginator;
        $this->request = $requestStack;
    }


    public function index(): Response
    {
        $dql = "SELECT o FROM App\Domain\Entities\Order o WHERE o.finished_at IS NOT NULL ORDER BY o.finished_at DESC";
        $query = $this->em->createQuery($dql);

        $pagination = $this->paginator->paginate(
            $query,
            $this->request->getCurrentRequest()->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('dashboard_order_pages.html.twig', ['pagination' => $pagination]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Pizza Plaza');
    }

    public function configureMenuItems(): iterable
    {
        return [];
    }
}
