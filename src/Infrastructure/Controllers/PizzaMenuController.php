<?php


namespace App\Infrastructure\Controllers;

use App\Application\Factory\OrderFactory;
use App\Application\Repositories\PizzaRepository;
use App\Application\Services\ExtraFormsGenerator;
use App\Application\Services\PizzaFormsGenerator;
use App\Application\Services\RemoveOrderedPizzaFormsGenerator;
use App\Infrastructure\Services\BreadcrumbBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

class PizzaMenuController extends AbstractController
{
    const GENERAL_PIZZA_ORDERING_TEMPLATE = 'pizza_options.html.twig';

    public function showPizzaOptions(Request $request,
                                     OrderFactory $orderFactory,
                                     PizzaRepository $pizzaRepository,
                                     PizzaFormsGenerator $pizzaFormsGenerator,
                                     ExtraFormsGenerator $extraFormsGenerator,
                                     RemoveOrderedPizzaFormsGenerator $removeOrderedPizzaFormsGenerator,
                                     RouterInterface $router,
                                     BreadcrumbBuilder $breadcrumbBuilder)
    {
        $pizzasArray = (!empty($result = $pizzaRepository->getAllPizzas())) ? $result : array();
        $arrayPizzaForms = (!empty($pizzasArray)) ? $pizzaFormsGenerator->getAllPizzaForms($pizzasArray) : array();
        $order = $orderFactory->generateOrder();
        $orderedPizzasArray = (!empty($result = $order->getOrderedPizzas()->toArray())) ? $result : array();
        $extrasForms = (!empty($orderedPizzasArray)) ? $extraFormsGenerator->getAllExtraForms($orderedPizzasArray) : array();
        $removeOrderedPizzasForms = (!empty($orderedPizzasArray)) ? $removeOrderedPizzaFormsGenerator->getAllRemoveOrderedPizzaForms($orderedPizzasArray) : array();
        $pathUri = $request->getRequestUri();

        $breadcrumbArray = array("Online ordering"=>"online_ordering");
        $breadcrumbBuilder->breadCrumbwithPrependHome($breadcrumbArray);

        $renderArray = array_merge(
            array('pizzasArray' => $pizzasArray),
            array('orderedPizzasArray' => $orderedPizzasArray),
            array('order' => $order),
            array('pathUri' => $pathUri),
            array('checkoutRoute' => $router->getRouteCollection()->get('checkout')->getPath()),
            $arrayPizzaForms,
            $extrasForms,
            $removeOrderedPizzasForms
        );

        return $this->render(self::GENERAL_PIZZA_ORDERING_TEMPLATE, $renderArray);
    }
}
