<?php


namespace App\Domain\ValueObjects;


class IdentificationNumber
{
    private $id;

    /**
     * IdentificationNumber constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->setId($id);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}