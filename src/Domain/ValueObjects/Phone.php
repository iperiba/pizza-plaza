<?php

namespace App\Domain\ValueObjects;

class Phone
{
    private $phoneNumber;

    public function __construct($phoneNumber)
    {
        $this->setPhoneNumber($phoneNumber);
    }

    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }


}