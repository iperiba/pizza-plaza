<?php

namespace App\Domain\ValueObjects;

class Price
{
    private $amount;

    public function __construct($amount)
    {
        $this->setAmount($amount);
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = floatval($amount);
    }


}