<?php

namespace App\Domain\ValueObjects;

class Address
{
    private $street;
    private $streetNumber;
    private $zip;
    private $city;

    public function __construct($street, $streetNumber, $zip, $city)
    {
        $this->setStreet($street);
        $this->setStreetNumber($streetNumber);
        $this->setZip($zip);
        $this->setCity($city);
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return mixed
     */
    public function getStreetNumber()
    {
        return $this->streetNumber;
    }

    /**
     * @param mixed $streetNumber
     */
    public function setStreetNumber($streetNumber)
    {
        $this->streetNumber = intval($streetNumber);
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    public function __toString()
    {
        return $this->getStreet() . ", " . $this->getStreetNumber() . " - " . $this->getCity() . ", " . $this->getZip();
    }
}