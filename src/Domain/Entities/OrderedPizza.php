<?php


namespace App\Domain\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class OrderedPizza
{
    private $id;
    private ?Order $order = null;
    private ?Pizza $pizza = null;
    private Collection $extras;

    /**
     * OrderedPizza constructor.
     * @param Collection $extras
     */
    public function __construct()
    {
        $this->extras = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return Pizza
     */
    public function getPizza()
    {
        return $this->pizza;
    }

    /**
     * @param Order|null $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @param Pizza|null $pizza
     */
    public function setPizza($pizza)
    {
        $this->pizza = $pizza;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getExtras()
    {
        return $this->extras;
    }

    /**
     * @param ArrayCollection|Collection $extras
     */
    public function setExtras($extras): void
    {
        $this->extras = $extras;
    }

    public function addExtra(Extra $extra)
    {
        $this->extras->add($extra);
    }

    public function removeExtra(Extra $extra)
    {
        $this->extras->removeElement($extra);
    }
}