<?php

namespace App\Domain\Entities;

use App\Domain\ValueObjects\Price;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Pizza
{
    private $id;
    private $name;
    private $description;
    private Price $price;
    private Collection $orderedPizzas;
    private Collection $extras;

    public function __construct($name, $description, Price $price)
    {
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
        $this->orderedPizzas = new ArrayCollection();
        $this->extras = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return Price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param Price $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getOrderedPizzas()
    {
        return $this->orderedPizzas;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getExtras()
    {
        return $this->extras;
    }

    /**
     * @param ArrayCollection|Collection $extras
     */
    public function setExtras($extras)
    {
        $this->extras = $extras;
    }

    public function addNewExtraToPizza(Extra $extra)
    {
        $this->extras->add($extra);
    }

    public function countNumberExtras(): int
    {
        return count($this->extras);

    }
}