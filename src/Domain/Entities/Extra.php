<?php

namespace App\Domain\Entities;

use \App\Domain\ValueObjects\Price;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Extra
{
    private $id;
    private $name;
    private Price $price;
    private $isChoosable;

    /**
     * Extra constructor.
     * @param $name
     * @param $price
     * @param $isChoosable
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getIsChoosable()
    {
        return $this->isChoosable;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param Price $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @param mixed $isChoosable
     */
    public function setIsChoosable($isChoosable)
    {
        $this->isChoosable = $isChoosable;
    }
}