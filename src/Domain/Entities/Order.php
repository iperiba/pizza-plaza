<?php

namespace App\Domain\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Order
{
    private $id;
    private $created_at;
    private $finished_at;
    private ?Customer $customer = null;
    private Collection $orderedPizzas;
    private float $total_price;

    public function __construct($id)
    {
        $this->setId($id);
        $this->setCreatedAt(new \Datetime());
        $this->orderedPizzas = new ArrayCollection();
        $this->setTotalPrice(0);
    }


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getFinishedAt()
    {
        return $this->finished_at;
    }

    /**
     * @param mixed $finished_at
     */
    public function setFinishedAt($finished_at)
    {
        $this->finished_at = $finished_at;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getOrderedPizzas()
    {
        return $this->orderedPizzas;
    }

    public function addOrderedPizza(OrderedPizza $orderedpizza)
    {
        $this->orderedPizzas->add($orderedpizza);
    }

    /**
     * @return float
     */
    public function getTotalPrice(): float
    {
        return $this->total_price;
    }

    /**
     * @param float $total_price
     */
    public function setTotalPrice(float $total_price): void
    {
        $this->total_price = $total_price;
    }

    public function addToTotalPrice($price): void
    {
        $this->total_price += floatval($price);
    }

    public function removeFromTotalPrice($price): void
    {
        $this->total_price -= floatval($price);
    }
}