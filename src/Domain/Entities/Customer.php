<?php

namespace App\Domain\Entities;

use \App\Domain\ValueObjects\Address;
use \App\Domain\ValueObjects\Phone;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;


class Customer
{
    private $id;
    private $firstName;
    private $lastName;
    private ?Address $address = null;
    private ?Phone $phone = null;
    private Collection $orders;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @param Address|null $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @param Phone|null $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param Phone|null $phone
     */
    public function addNewRelatedOrder(Order $order)
    {
        $this->orders->add($order);
    }
}