<?php


namespace App\Domain\Services;


class OrderedPizzaArrayOperator implements DomainServiceInterface
{
    public function getArrayOrderedPizzasIds(array $orderedPizzasArray)
    {
        $idOrderedPizzasArray = array();

        foreach ($orderedPizzasArray as $orderedPizza) {
            array_push($idOrderedPizzasArray, $orderedPizza->getId());
        }

        return $idOrderedPizzasArray;
    }
}