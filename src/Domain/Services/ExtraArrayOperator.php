<?php

namespace App\Domain\Services;


class ExtraArrayOperator implements DomainServiceInterface
{
    public function getArrayExtraIds(array $extrasArray)
    {
        $idExtraArray = array();

        foreach ($extrasArray as $extra) {
            array_push($idExtraArray, $extra->getId());
        }

        return $idExtraArray;
    }
}