<?php

namespace App\Domain\Services;


class PizzaArrayOperator implements DomainServiceInterface
{
    public function getArrayPizzaIds(array $pizzasArray)
    {
        $idPizzaArray = array();

        foreach ($pizzasArray as $pizza) {
            array_push($idPizzaArray, $pizza->getId());
        }

        return $idPizzaArray;
    }
}